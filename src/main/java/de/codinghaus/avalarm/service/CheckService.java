package de.codinghaus.avalarm.service;

import de.codinghaus.avalarm.config.ConfigProperties;
import de.codinghaus.avalarm.model.Results;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CheckService {

    private final ConfigProperties configProperties;
    private final Results results;
    private final DiscordClient discordClient;
    private int snoozeCounter = 0;
    private final HttpClient client = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(50))
            .build();

    @PostConstruct
    public void setup() {
        System.out.printf("Website monitored: '%s' (Interval: %ds, Consecutive failures needed for alarm: %d)%n",
                configProperties.getUrl(), configProperties.getIntervalInSeconds(), configProperties.getMaxFailedRequests());
    }

    @Scheduled(fixedRateString = "${check.interval_in_ms}")
    public void checkUrl() throws IOException, InterruptedException {
        if (snoozeCounter > 0) {
            snoozeCounter--;
            System.out.println(String.format("Snoozing... will snooze %d more times.", snoozeCounter));
            return;
        }

        final HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(configProperties.getUrl()))
                .build();
        boolean success;
        try {
            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            success = results.process(response.statusCode());
        } catch (HttpTimeoutException e) {
            success = results.process(Results.TIMEOUT);
        }
        if (results.alarm()) {
            discordClient.informAboutAlarm(results.result());
            snoozeTimes(configProperties.getSnoozeCounter());
            results.reset();
        } else {
            if (success) {
                System.out.println("all good. (" + LocalDateTime.now() + ")");
            }
        }
    }

    private void snoozeTimes(int times) {
        snoozeCounter = times;
    }
}
