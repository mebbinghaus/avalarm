package de.codinghaus.avalarm;

import de.codinghaus.avalarm.service.DiscordClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class AvalarmApplicationTests {

	@MockBean
	private DiscordClient discordClient;

	@Test
	void contextLoads() {
	}

}
